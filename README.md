# Projecte Metasploit

## Projecte final del grau d'ASIX.
## Curs 2019-2020.
## Pere Canet Pons

Aquest repositori es troba dividit en els següents directoris:

- Teoria: s'hi troben tots els documents que contenen la documentació i informació que tracto en el projecte.
- Pràctica: en aquest directori s'hi troben els documents descriptius de totes les pràctiques realitzades.
- Aux: fitxers auxiliars (headers, css, etc.)
- Configuracions: documents amb les configuracions necessàries de les eines utilitzades al projecte.
- Imatges: totes les imatges utilitzades en la presentació i les captures fetes al llarg de les pràctiques.
- Instal·lacions: documents amb les instal·lacions de les eines necessàries pel projecte.
- Poster-video: video introductori del projecte i el pòster en format pdf i odt.


## En què consisteix aquest projecte?

En aquest m'he volgut enfocar en l'eina Metasploit a fons, concretament en el seu subprojecte Metasploit Framework (MSF): entendre tant la seva arquitectura com el seu sistema d'arxius, veure part de les funcionalitats (perque en té moltes) que ens ofereix i comprendre la importància d'aquesta eina avui en dia.

D'altra banda, al projecte també he tractat més utilitats que he necessitat pel desenvolupament de les pràctiques, com és el sistema operatiu Kali Linux i l'eina Metasploitable 3. Sense aquests hauria d'haver enfocat les pràcitques d'una forma molt diferent, pel que els hi he donat el seu espai al treball.

## Estructura

### Metasploit

- Edicions
- MSF
- Arquitectura
- Mòduls
- Sistema d'arxius
- Comandes bàsiques
- Consola de MSF
- Què és un exploit?
- Què es pot fer amb MSF?
- Pentesting
- Metodologia de MSF

![](imatges/logo1.png)

### Eines complementàries

#### Kali Linux
- Explicació general

![](imatges/kali-linux.png)

#### Metasploitable 3
- Explicació general

![](imatges/metasploitable-3.png)

### Instal·lacions

- Oracle Virtualbox
- Metasploit
- Kali Linux
- Metasploitable 3

## Part pràctica

- Recopilació d'informació d'un sistema
- Explotació d'un servidor web Apache
- Explotació d'un servei Glassfish
- Explotació d'un servei Jenkins
- Explotació d'SSH