# Instal·lació de Kali Linux a Virtualbox

1. Accedim al [git oficial de Metasploitable3](https://github.com/rapid7/metasploitable3) i allà hi trobem els passos a seguir per fer la instal·lació de la màquina virtual
> ![](../imatges/metasploitable1.png)

2. Així doncs, en primer lloc creem el directori *metasploitable3-workspace* i després ens hi col·loquem a dins:

> ![](../imatges/metasploitable2.png)

3. Un cop fet, executem la comanda *curl -O https://raw.githubusercontent.com/rapid7/metasploitable3/master/Vagrantfile && vagrant up*:

> ![](../imatges/metasploitable3.png)

4. Llest! Ja tenim instal·lades les VMs de Metasploitable3 (en el nostre cas només ens cal la de Windows):

> ![](../imatges/metasploitable4.png)

5. Podem engegar el Metasploitable de Windows per veure que tot funciona correctament:

> ![](../imatges/metasploitable5.png)