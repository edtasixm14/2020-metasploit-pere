# Instal·lació de Kali Linux a Virtualbox

1. Accedim a la [web oficial de Kali](https://www.kali.org/downloads/) i descarreguem la imatge del sistema operatiu:
> ![](../imatges/kali1.png)

> ![](../imatges/kali2.png)

2. Un cop fet, creem una màquina virtual pel Kali Linux:

> ![](../imatges/kali3.png)

3. A l'apartat d'emmagatzematge hi col·loquem la imatge descarregada:

> ![](../imatges/kali4.png)

4. Llest! Ja podem iniciar la màquina virtual de Kali:

> ![](../imatges/kali5.png)