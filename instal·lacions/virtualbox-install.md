# Instal·lació de Oracle Virtualbox

1. Ens assegurem que tinguem el sistema actualitzat.

> ![](../imatges/1-update.png)

2. Instal·lem les dependències.

> ![](../imatges/2-dependencies.png)

> ![](../imatges/3-dependencies.png)

3. Instal·lem Virtualbox.

> ![](../imatges/4-install.png)

4. Llest! Ja tenim el programa a punt.

> ![](../imatges/5-oracle.png)