# Kali Linux

És una distribució basada en Debian GNU/Linux dissenyada, principalment, per a l'auditoria i seguretat informàtica en general.

Aquesta porta preinstal·lats més de 600 programes incloent Nmap (un escàner de ports), Wireshark (un sniffer) i John the Ripper (un crackejador de passwords), per exemple. Entre aquests s'hi troba Metasploit, raó per la qual he escollit utilitzar aquesta distribució.

Kali pot ser usat des d'un Live CD, live-usb, instal·lada com a sistema operatiu principal i, com en el meu cas, des d'una màquina virtual.

Per últim, aquest sistema operatiu és desenvolupat en un entorn segur; l'equip de Kali està compost per un grup petit de persones de confiança, els quals són els que tenen permès modificar paquets i interactuar amb els repositoris oficials.

Tots els paquets de Kali estan signats per cada desenvolupador que ho va compilar i publicar. Al seu torn, els encarregats de mantenir els repositoris també signen posteriorment els paquets utilitzant GNU Privacy Guard.

![](../imatges/captura-kali.png)