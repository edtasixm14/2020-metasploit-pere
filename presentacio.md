### Metasploit

+ Projecte de codi obert de seguretat informàtica. Ens proporciona informació sobre vulnerabilitats de seguretat, ajuda en tests de penetració i el desenvolupament de signatures per a sistemes de detecció d'intrusos.

![](imatges/logo1.png)

### Edicions

+ Metasploit ens ofereix diferents versions entre les quals podem escollir:
    + Metasploit Framework
    + Metasploit Community Edition
    + Metasploit Express
    + Metasploit Pro

![](imatges/edicions.png)

### MSF

+ Metasploit Framework (MSF) és el subprojecte més conegut.
+ La major part està desenvolupada en Perl i Ruby.
+ S'enfoca en auditors de seguretat i equips Red Team i Blue Team, sobretot.

    + Red Team és l'equip ofensiu o encarregat del 'hacking' ètic, que fa proves d'intrusió.
    + Blue Team és l'equip que porta a terme la securització i tota la part defensiva.

![](imatges/red-blue-team.png)

### Arquitectura de MSF

![](imatges/arquitectura.png)

### Mòduls

+ Són peces o blocs de codi que implementen una o varies funcionalitats, com l'execució d'un exploit concret o la realització d'un escaneig de màquines remotes.
+ MSF disposa de molts mòdulos, els quals ajuden a augmentar de manera senzilla les funcionalitats y l'úso d'aquesta eina.
+ En trobem diferents tipus:

    + Exploits
    + Payloads
    + Nops
    + Encoders
    + Post
    + Auxiliarys

### Sistema d'arxius

Directoris que es troben a la ruta /usr/share/metasploit-framework:

+ .data
+ .documentation
+ .lib
+ .modules
+ .tools
+ .scripts
+ .plugins

![](imatges/sistema-arxius.png)

### Comandes bàsiques de MSF

+ msfupdate: actualitzar Metasploit a la seva última versió.
+ msfvenom: crear BackDoors (encoders, payloads, etc.).
+ msfconsole: ingressar a la consola de Metasploit.

![](imatges/msfvenom.png)

### Comandes bàsiques de la consola de MSF

+ Les ordres que hem de conèixer són les següents:

    + help
    + search
    + use
    + info
    + show
    + set
    + check
    + run/exploit

### Què és un exploit?

+ Peça de programari o codi que aprofita un forat de seguretat en una aplicació o sistema.
+ Dos tipus d'exploits:

    + Coneguts
    + Desconeguts

![](imatges/exploit.png)

### Què es pot fer amb MSF?

+ Escanejar i recopilar tota la informació d'una màquina
+ Identificar i explotar vulnerabilitats
+ Escalament de privilegis i robatori de dades
+ Instalació d'una 'backdoor'
+ Fuzzing
+ Escapar d'antivirus
+ Eliminació de registres i traces

![](imatges/pentest.png)

### Pentesting

+ Una prova de penetració és un atac a un sistema informàtic amb la intenció de trobar les debilitats de seguretat i tot el que podria tenir accés a aquest, la seva funcionalitat i dades.

![](imatges/vulnerabilitat.png)

### Metodologia de MSF

+ Part més interessant de Metasploit: el seu ús.

+ Aquests són els passos bàsics per operar un sistema amb Metasploit:

    + Recopilar informació sobre l'objectiu, com la versió de sistema operatiu i els serveis de xarxa instal·lats.
    + Comprovar els exploits als que el sistema de destinació és sensible.
    + Triar i configurar un exploit.
    + Triar i configurar una càrrega útil.
    + Executar l'exploit.

![](imatges/exploit-metodologia.png)

### Eines complementàries

+ Kali Linux
+ Metasploitable 3

![](imatges/kali-metasploitable.png)

### Kali Linux

+ Distribució basada en Debian GNU/Linux.
+ Dissenyada, principalment, per a l'auditoria i seguretat informàtica en general.
+ Porta preinstal·lats més de 600 programes, incloent Metasploit Framework entre ells.

![](imatges/captura-kali.png)

### Metasploitable 3

+ Màquina virtual preconfigurada que compta amb una sèrie de configuracions i vulnerabilitats pensades per permetre'ns depurar les nostres tècniques de hacking.
+ Aquesta tercera versió depèn de Vagrant i Packer per compilar la imatge en el sistema i, així, ser molt més dinàmica.

![](imatges/metasploitable3.png)

### Instal·lacions

+ Oracle Virtualbox
+ Metasploit
+ Kali Linux
+ Metasploitable 3

![](imatges/kali-metasploit.png)

### FI

Moltes gràcies per la vostra atenció.

Anem amb la part pràctica!

![](imatges/logo3.png)
